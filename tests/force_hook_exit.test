<?php

/**
 * @file
 * Tests for force_hook_exit.
 */

/**
 * Test basic functionality.
 */
class ForceHookExitWebTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => 'Basic functionality',
      'description' => 'Tests the basic functionality of force_hook_exit.',
      'group' => 'Force hook exit',
    );
  }

  public function setUp() {
    parent::setUp('force_hook_exit', 'force_hook_exit_test');
  }

  /**
   * Basic tests.
   */
  public function test() {
    $this->drupalGet('force_hook_exit_test_exiting_page');
    $this->assertText('I am a test page that exits.');

    $this->refreshVariables();
    $this->assertIdentical(1, variable_get('force_hook_exit_test_fired', 0), 'hook_exit() was called once.');

    // Check that we have cd'd back to the doc root.
    $this->assertEqual(getcwd(), variable_get('force_hook_exit_test_directory', ''), 'Made it to the docroot.');

    // Check that it's still called only once on a regular page load.
    $this->drupalGet('');
    $this->refreshVariables();
    $this->assertIdentical(1, variable_get('force_hook_exit_test_fired', 0), 'hook_exit() was called once.');
  }

}
